## Description
Helper to insert BIRT Report rptdesign files into ElasticSearch

## Usage
`npm i`

then...

`node index.js ../birt-reports/es_connected.rptdesign [http://10.4.33.123:9200]`

The final argument is OPTIONAL (default is localhost:9200) and is the URL for the target ES instance.

## Sample Reports
A few sample report files are present in ./birt-reports/

The "es_connected" report is currently still a work-in-progress.