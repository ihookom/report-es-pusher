const fs = require('fs');
const axios = require('axios');

const printUsageAndExit = (error) => {
  console.log('ERROR: ' + error);
  console.log('USAGE: node index.js reports/example.rptdesign [http://10.9.34.222:9200]');
  process.exit(1);
}

let file = process.argv[2];
if (!file || !fs.existsSync(file) || !file.endsWith('.rptdesign')) {
  printUsageAndExit('Please provide path to existing rptdesign file');
}

let esReportDesignUrl = 'http://localhost:9200';
if (process.argv[3]) {
  esReportDesignUrl = process.argv[3];
}
esReportDesignUrl += '/report/report-design/';

let reportXml = fs.readFileSync(file, {encoding: 'utf-8'});
let begin = file.lastIndexOf('/') > 0 ? file.lastIndexOf('/') + 1 : 0;
let reportName = file.substring(begin, file.indexOf('.rptdesign'));

let body = {
  user: 'cequence',
  createdDate: Date.now(),
  reportName: reportName,
  content: reportXml.toString()
};

let headers = {
  'content-type': 'application-json',
  'cache-control': 'no-cache'
};

axios.post(`${esReportDesignUrl}${reportName}`, body, {headers: headers})
  .then(res => { console.log(res) })
